//1

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
]

//Класс списку книг
class Books {
    
    //Конструктор для створення списку
    constructor (Array){
        let divBooks = document.createElement('div')
        document.body.appendChild(divBooks)
        this.ulBooks = document.createElement('ul')
        divBooks.appendChild(this.ulBooks)
        this.Array = Array
    }
    
    //Метод для заповнення списку за вказаними умовами
    booksAdd() {
        //Перебір массиву з книгами
        this.Array.forEach(element=>{
            let name = element.name
            let author = element.author
            let price = element.price

            //Створення помилок..
            try{
                
                //..якщо автор не вказаний
                if(!element.author) {
                    throw new Error(element.name + " Автор книги не вказаний")
                }
                //..якщо ціна не вказана
                if(!element.price) {
                    throw new Error(element.name + " Ціна книги не вказана")
                }
                //заповнення списку якщо всі данні вказані
                else {
                    let liBook = document.createElement('li')
                    this.ulBooks.appendChild(liBook)
                    liBook.textContent = `Книга ${name}, за авторством ${author} по ціні ${price}`
                }
            }
            //Ловимо помилки і видаємо іх в консоль
            catch(err) {
                console.log(err)
            }

        })
    }
}

//Створення списку
const booksUl = new Books(books)
//Заповнення списку
booksUl.booksAdd()
